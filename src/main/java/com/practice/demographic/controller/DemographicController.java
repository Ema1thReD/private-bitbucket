package com.practice.demographic.controller;

import com.practice.demographic.entity.Demographic;
import com.practice.demographic.repository.DemographicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public  class DemographicController
{
    @Autowired
    private DemographicRepository demographicRepository;


    @GetMapping("/getAll")
    public List<Demographic> getAll() {
        return demographicRepository.findAll();
    }

    @GetMapping("/get/{id}")
    public List<Demographic>getId
            (@PathVariable(value = "id")Integer id){
        return demographicRepository.findAllById(id);
    }

    @PostMapping("/create")
    public Demographic createDemographic(@Valid @RequestBody Demographic demographic){
        return demographicRepository.save(demographic);
    }

    @RequestMapping("/byLastName/{lastName}")
    public List<Demographic>bylastName(@PathVariable(value="lastName") String lastName){
        return demographicRepository.findByLastName(lastName);
    }
    @PostMapping("/update")
    public Map<String, String> updateBook(@RequestBody Demographic demographic) {

        Map<String, String> message = new HashMap<>();

        if(demographic.getId() != null){
            Optional<Demographic> demographicOptional = demographicRepository.findByIdAndActive(demographic.getId(), true);

            Boolean flag = demographicOptional.isPresent();
            if(flag){
                demographicRepository.save(demographic);

            } else{
                message.put("message", "details not found");
            }
            message.put("Id", "" + demographic.getId());
            message.put("Updated", "" + flag);

        } else{
            message.put("message", "id not sent");
        }


        return message;

    }
    // Delete operation by id
    @PostMapping("/delete/{id}")
    public Map<String,String>  deleteDemographic(@PathVariable(value = "id") Integer id) {

        Map<String,String> message = new HashMap<>();

        if(id != null){
            Optional<Demographic> demographicOptional = demographicRepository.findByIdAndActive(id, true);

            Boolean flag = demographicOptional.isPresent();
            if(flag){
                Demographic demographic= demographicOptional.get();
                demographic.setActive(false);
                demographicRepository.save(demographic);

            } else{
                message.put("message", "demographic details  not found");
            }
            message.put("Id", "" + id);
            message.put("Deleted", "" + flag);

        } else{
            message.put("message", "id not sent");
        }
        return message;
    }

//     //Delete a demographic detail
//    @DeleteMapping("/delete/{id}")
//    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Integer id) {
//        Demographic demographic;
//        demographic = demographicRepository.findById(id)
//                .orElseThrow(() -> {
//                    return new ResourceRequestDeniedException( "Demographic");
//                });
//
//        demographicRepository.delete(demographic);
//
//        return ResponseEntity.ok().build();}
}