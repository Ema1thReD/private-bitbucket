package com.practice.demographic.repository;

import com.practice.demographic.entity.Demographic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DemographicRepository extends JpaRepository<Demographic,Integer> {


    List<Demographic> findAllById(Integer id);
    List<Demographic> findByLastName(String lastName);
    Optional<Demographic> findByIdAndActive(Integer id,Boolean actve);
}
